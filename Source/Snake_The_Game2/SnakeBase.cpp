// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::DOWN;

	MovementSpeed = 1.f;
	MaxSpeed = 0.1f;
	MinSpeed = 1.f;
	SpeedExpansion = 0;

	AmountOfElements = 4;
	IsEnableMove = true;
	
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(AmountOfElements);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
	SpeedExpansion++;
	if (SpeedExpansion == 3)
	{
		SpeedExpansion = 0;
			if (MovementSpeed > MaxSpeed)
			{
				MovementSpeed = MovementSpeed - 0.3f;
			}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(0, 0, 0);
	
		switch (LastMoveDirection)
		{
			case EMovementDirection::UP:
			{	
				
				if(IsEnableMove) MovementVector.X = MovementVector.X + ElementSize;
				IsEnableMove = false;
				
			}break;

			case EMovementDirection::DOWN:
			{
				
				if (IsEnableMove) MovementVector.X = MovementVector.X - ElementSize;
				IsEnableMove = false;

			}break;

			case EMovementDirection::LEFT:
			{
				
				if (IsEnableMove) MovementVector.Y = MovementVector.Y + ElementSize;
				IsEnableMove = false;
				
			}break;

			case EMovementDirection::RIGHT:
			{
				
				if (IsEnableMove) MovementVector.Y = MovementVector.Y - ElementSize;
				IsEnableMove = false;
				
			}break;

		}
	
	
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();

	IsEnableMove = true;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

float ASnakeBase::MovementSpeedValue()
{
	return MovementSpeed;
}

