// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snake_The_Game2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_THE_GAME2_API ASnake_The_Game2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
