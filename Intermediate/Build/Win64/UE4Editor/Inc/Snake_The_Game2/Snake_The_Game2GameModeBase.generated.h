// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_THE_GAME2_Snake_The_Game2GameModeBase_generated_h
#error "Snake_The_Game2GameModeBase.generated.h already included, missing '#pragma once' in Snake_The_Game2GameModeBase.h"
#endif
#define SNAKE_THE_GAME2_Snake_The_Game2GameModeBase_generated_h

#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_SPARSE_DATA
#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_RPC_WRAPPERS
#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnake_The_Game2GameModeBase(); \
	friend struct Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnake_The_Game2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_The_Game2"), NO_API) \
	DECLARE_SERIALIZER(ASnake_The_Game2GameModeBase)


#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnake_The_Game2GameModeBase(); \
	friend struct Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnake_The_Game2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_The_Game2"), NO_API) \
	DECLARE_SERIALIZER(ASnake_The_Game2GameModeBase)


#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnake_The_Game2GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnake_The_Game2GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake_The_Game2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake_The_Game2GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake_The_Game2GameModeBase(ASnake_The_Game2GameModeBase&&); \
	NO_API ASnake_The_Game2GameModeBase(const ASnake_The_Game2GameModeBase&); \
public:


#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnake_The_Game2GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake_The_Game2GameModeBase(ASnake_The_Game2GameModeBase&&); \
	NO_API ASnake_The_Game2GameModeBase(const ASnake_The_Game2GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake_The_Game2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake_The_Game2GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnake_The_Game2GameModeBase)


#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_12_PROLOG
#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_SPARSE_DATA \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_RPC_WRAPPERS \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_INCLASS \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_SPARSE_DATA \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_THE_GAME2_API UClass* StaticClass<class ASnake_The_Game2GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_The_Game2_Source_Snake_The_Game2_Snake_The_Game2GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
