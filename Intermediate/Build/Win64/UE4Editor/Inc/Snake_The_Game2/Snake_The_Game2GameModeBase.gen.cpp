// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake_The_Game2/Snake_The_Game2GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnake_The_Game2GameModeBase() {}
// Cross Module References
	SNAKE_THE_GAME2_API UClass* Z_Construct_UClass_ASnake_The_Game2GameModeBase_NoRegister();
	SNAKE_THE_GAME2_API UClass* Z_Construct_UClass_ASnake_The_Game2GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Snake_The_Game2();
// End Cross Module References
	void ASnake_The_Game2GameModeBase::StaticRegisterNativesASnake_The_Game2GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ASnake_The_Game2GameModeBase_NoRegister()
	{
		return ASnake_The_Game2GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake_The_Game2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Snake_The_Game2GameModeBase.h" },
		{ "ModuleRelativePath", "Snake_The_Game2GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnake_The_Game2GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics::ClassParams = {
		&ASnake_The_Game2GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnake_The_Game2GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASnake_The_Game2GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASnake_The_Game2GameModeBase, 262214916);
	template<> SNAKE_THE_GAME2_API UClass* StaticClass<ASnake_The_Game2GameModeBase>()
	{
		return ASnake_The_Game2GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASnake_The_Game2GameModeBase(Z_Construct_UClass_ASnake_The_Game2GameModeBase, &ASnake_The_Game2GameModeBase::StaticClass, TEXT("/Script/Snake_The_Game2"), TEXT("ASnake_The_Game2GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnake_The_Game2GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
